//
//  AppDelegate.swift
//  Colonise
//
//  Created by Rob van der Veer on 30/7/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

import Cocoa

class AppDelegate: NSObject, NSApplicationDelegate {
                            
    @IBOutlet weak var window: NSWindow!


    func applicationDidFinishLaunching(aNotification: NSNotification?) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification?) {
        // Insert code here to tear down your application
    }

    func applicationShouldTerminateAfterLastWindowClosed(sender: NSApplication!) -> Bool
    {
        return true
    }
}


//
//  DragDropView.swift
//  Colonise
//
//  Created by Rob van der Veer on 30/7/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

import Cocoa

class DragDropView: NSView
{
    var highlight = false;
//    
    init(frame: NSRect)
    {
        super.init(frame: frame)
        // Initialization code here.
        self.registerForDraggedTypes([NSFilenamesPboardType])
    }
    
    init(coder: NSCoder!)
    {
        super.init(coder: coder)
        self.registerForDraggedTypes([NSFilenamesPboardType])
    }
    
    override func draggingEntered(sender: NSDraggingInfo!) -> NSDragOperation
    {
        highlight = true
        needsDisplay = true
        
        return NSDragOperation.Generic
    }
    
    override func draggingExited(sender: NSDraggingInfo!)
    {
        highlight = false
        needsDisplay = true
    }
    
    override func prepareForDragOperation(sender: NSDraggingInfo!) -> Bool
    {
        highlight = false
        needsDisplay = true
        return true
    }
    
    override func performDragOperation(sender: NSDraggingInfo!) -> Bool
    {
        let draggedFileNames : AnyObject! = sender.draggingPasteboard().propertyListForType(NSFilenamesPboardType)
        
        let filenames = draggedFileNames as [AnyObject]
        let filename = filenames[0] as String
        let fileExtension = filename.pathExtension
        
        if fileExtension == "txt" || fileExtension == "csv"
        {
            return true
        }
        
        let alert = NSAlert()
        alert.messageText = "Colonise cannot convert this file"
        alert.informativeText = "Only file with .txt or .csv extension are supported"
        alert.runModal();
        
        return false
    }
    
    override func concludeDragOperation(sender: NSDraggingInfo!)
    {
        let draggedFileNames : AnyObject! = sender.draggingPasteboard().propertyListForType(NSFilenamesPboardType)
        
        let filenames = draggedFileNames as [AnyObject]
        let filename = filenames[0] as String
        
        let textDataFile = NSString.stringWithContentsOfFile(filename, encoding: NSASCIIStringEncoding, error: nil) as String
        

        //hack away.
        let regex = NSRegularExpression.regularExpressionWithPattern("(,)(?=(?:[^\"]|\"[^\"]*\")*$)", options: nil, error: nil)
            
        //split the file into lines.
        let lines = textDataFile.componentsSeparatedByString("\n")
        
        var outputLines = [String]()
        
        for line in lines
        {
            //use regex to split quoted strings.
            let outputLine = regex.stringByReplacingMatchesInString(line, options: nil, range: NSRange(location: 0, length: countElements(line)), withTemplate: ";")
            
            NSLog("< %@", line)
            NSLog("> %@", outputLine)
            
            outputLines.append(outputLine)
        }
        
        let folder = filename.stringByDeletingLastPathComponent
        let newFileName = filename.lastPathComponent.stringByDeletingPathExtension
        let newPath = NSString.pathWithComponents([folder, newFileName + "_colonised." + filename.pathExtension])
        
        let newData = "\n".join(outputLines.map({ "\($0)" }))
        newData.writeToFile(newPath, atomically: false, encoding: NSUTF8StringEncoding, error: nil);

    }
    
    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)
        
        if ( highlight )
        {
            NSColor(white: 1, alpha: 0.5).setFill()
            
            NSRectFillUsingOperation(dirtyRect, NSCompositingOperation.CompositeScreen)
            
            
            NSColor.grayColor().set()
            
            NSBezierPath.setDefaultLineWidth(5)
            NSBezierPath.strokeRect(self.bounds)
        }
        else
        {
        }
    }

}

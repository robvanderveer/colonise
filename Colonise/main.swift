//
//  main.swift
//  Colonise
//
//  Created by Rob van der Veer on 30/7/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

import Cocoa

NSApplicationMain(C_ARGC, C_ARGV)
